## Setup
```
pip install -r requirements.txt
python manage.py migrate
```

## Start server
```
python manage.py runserver
```

## Testing
```
python manage.py test
```
