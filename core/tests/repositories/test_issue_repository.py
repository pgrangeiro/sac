# coding: utf-8
from mock import call, patch, Mock
from unittest import TestCase

from core.reporitories import IssueReposity


class IssueReposityTestCase(TestCase):

    def setUp(self):
        patcher = patch('core.reporitories.issue_repository.IssueFactory')

        self.factory = patcher.start() # TODO: Mock behavior
        self.dao = Mock()
        self.repository = IssueReposity(self.dao)

        self.dao.all.return_value = [1, 2]
        self.addCleanup(patcher.stop)

    def test_initializes_repository_correctly(self):
        self.assertEqual(self.dao, self.repository.dao)

    def test_list_calls_dao_correctly(self):
        list(self.repository.list())
        self.assertTrue(self.dao.all.called)

    def test_list_calls_factory_correctly(self):
        self.factory.create.side_effect = ['1', '2']

        result = list(self.repository.list())
        self.assertEqual(2, self.factory.create.call_count)
        self.factory.create.assert_has_calls([call(1), call(2)])
        self.assertEqual(['1', '2'], result)
