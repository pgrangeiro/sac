# coding: utf-8
from datetime import date
from unittest import TestCase

from core.factories import IssueFactory


class IssueFactoryTestCase(TestCase):

    def test_create_instantiates_domain_obj_correctly(self):
        data = {
            'id': 1,
            'source_id': 2,
            'location_id': 3,
            'subject_id': 4,
            'description': u'Description',
            'created_at': date(2015, 1, 1),
            'finished_at': date(2015, 1, 2),
        }
        obj = IssueFactory.create(data)

        self.assertEqual(obj.id, data['id'])
        self.assertEqual(obj.source_id, data['source_id'])
        self.assertEqual(obj.location_id, data['location_id'])
        self.assertEqual(obj.subject_id, data['subject_id'])
        self.assertEqual(obj.description, data['description'])
        self.assertEqual(obj.created_at, data['created_at'])
        self.assertEqual(obj.finished_at, data['finished_at'])
