# coding: utf-8
from mock import Mock
from unittest import TestCase

from core.use_cases import CreateIssueUseCase


class CreateIssueUseCaseTestCase(TestCase):

    def setUp(self):
        self.dao = Mock()
        self.use_case = CreateIssueUseCase(self.dao)

    def test_initializes_use_case_correctly(self):
        self.assertEqual(self.dao, self.use_case.dao)

    def test_use_case_calls_dao_correctly(self):
        self.use_case.execute(1, 2, 3, 'Description')
        self.dao.create.assert_called_once_with(
            source_id=1,
            location_id=2,
            subject_id=3,
            description='Description',
        )
