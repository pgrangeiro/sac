# coding: utf-8
from mock import Mock
from unittest import TestCase

from core.use_cases import FinishIssueUseCase


class FinishIssueUseCaseTestCase(TestCase):

    def setUp(self):
        self.dao = Mock()
        self.use_case = FinishIssueUseCase(self.dao)

    def test_initializes_use_case_correctly(self):
        self.assertEqual(self.dao, self.use_case.dao)

    def test_use_case_calls_dao_correctly(self):
        self.use_case.execute(1)
        self.dao.finish.assert_called_once_with(1)
