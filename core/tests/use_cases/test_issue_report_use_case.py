# coding: utf-8
from mock import patch, Mock
from unittest import TestCase

from core.formatters import NullableFormatter
from core.reporitories import IssueReposity
from core.use_cases import IssueReportUseCase


class ReportIssueUseCaseTestCase(TestCase):

    def setUp(self):
        patchers = [
            patch('core.use_cases.issue_report_use_case.IssueReposity', Mock(IssueReposity)),
            patch('core.use_cases.issue_report_use_case.NullableFormatter', Mock(NullableFormatter)),
        ]

        self.repository = patchers[0].start()
        self.formatter = patchers[1].start()
        self.dao = Mock()
        self.use_case = IssueReportUseCase(self.dao)

        for p in patchers:
            self.addCleanup(p.stop)

    def test_initializes_use_case_correctly(self):
        self.repository.assert_called_once_with(self.dao)
        self.assertEqual(self.repository(), self.use_case.repository)
        self.assertEqual(self.formatter, self.use_case.formatter)

    def test_initializes_use_case_with_formatter_correctly(self):
        formatter = Mock()
        use_case = IssueReportUseCase(self.dao, formatter)
        self.assertEqual(formatter, use_case.formatter)

    def test_execute_calls_repository_correctly(self):
        self.use_case.execute()
        self.assertTrue(self.use_case.repository.list.called)

    def test_execute_calls_formatter_correctly(self):
        self.repository().list.return_value = 2
        self.formatter.format.return_value = 1

        result = self.use_case.execute()
        self.formatter.format.assert_called_once_with(2)
        self.assertEqual(1, result)
