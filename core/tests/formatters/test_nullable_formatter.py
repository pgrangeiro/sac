# coding: utf-8
from unittest import TestCase

from core.formatters import NullableFormatter


class NullableFormatterTestCase(TestCase):

    def test_format_returns_none(self):
        self.assertIsNone(NullableFormatter.format([]))
