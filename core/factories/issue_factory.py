# coding: utf-8
from core.domain import Issue


class IssueFactory(object):

    @classmethod
    def create(cls, data):

        return Issue(
            id=data['id'],
            source_id=data['source_id'],
            location_id=data['location_id'],
            subject_id=data['subject_id'],
            description=data['description'],
            created_at=data['created_at'],
            finished_at=data['finished_at'],
        )

