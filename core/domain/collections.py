# coding: utf-8

LOCATIONS = [
    (1, u'AC'),
    (2, u'AL'),
    (3, u'AP'),
    (4, u'AM'),
    (5, u'BA'),
    (6, u'CE'),
    (7, u'DF'),
    (8, u'ES'),
    (9, u'GO'),
    (10, u'MA'),
    (11, u'MT'),
    (12, u'MS'),
    (13, u'MG'),
    (14, u'PA'),
    (15, u'PB'),
    (16, u'PR'),
    (17, u'PE'),
    (18, u'PI'),
    (19, u'RJ'),
    (20, u'RN'),
    (21, u'RS'),
    (22, u'RO'),
    (23, u'RR'),
    (24, u'SC'),
    (25, u'SP'),
    (26, u'SE'),
    (27, u'TO'),
]

SOURCES = [
    (1, u'Chat'),
    (2, u'Email'),
    (3, u'Telefone'),
]

SUBJECTS = [
    (1, u'Dúvida'),
    (2, u'Elogio'),
    (3, u'Sugestão'),
]
