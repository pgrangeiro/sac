# coding: utf-8
from core.domain.collections import LOCATIONS, SOURCES, SUBJECTS


class Issue(object):

    def __init__(self, id, source_id, location_id, subject_id, description, created_at, finished_at=None):
        self.id = id
        self.source_id = source_id
        self.location_id = location_id
        self.subject_id = subject_id
        self.description = description
        self.created_at = created_at
        self.finished_at = finished_at

    @property
    def source(self):
        return dict(SOURCES)[self.source_id]

    @property
    def location(self):
        return dict(LOCATIONS)[self.location_id]

    @property
    def subject(self):
        return dict(SUBJECTS)[self.subject_id]
