# coding: utf-8


class CreateIssueUseCase(object):

    def __init__(self, dao):
        self.dao = dao

    def execute(self, source_id, location_id, subject_id, description):
        self.dao.create(
            source_id=source_id,
            location_id=location_id,
            subject_id=subject_id,
            description=description,
        )
