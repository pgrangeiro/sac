# coding: utf-8
from core.formatters import NullableFormatter
from core.reporitories import IssueReposity


class IssueReportUseCase(object):

    def __init__(self, dao, formatter=None):
        self.repository = IssueReposity(dao)
        self.formatter = formatter or NullableFormatter

    def execute(self):
        return self.formatter.format(self.repository.list())
