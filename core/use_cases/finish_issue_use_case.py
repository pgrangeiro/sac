# coding: utf-8


class FinishIssueUseCase(object):

    def __init__(self, dao):
        self.dao = dao

    def execute(self, issue_id):
        self.dao.finish(issue_id)
