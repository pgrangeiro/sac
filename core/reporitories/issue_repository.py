# coding: utf-8
from core.factories import IssueFactory


class IssueReposity(object):

    def __init__(self, dao):
        self.dao = dao

    def list(self):
        for item in self.dao.all():
            yield IssueFactory.create(item)
