# coding: utf-8
from itertools import groupby


class AggregateIssues(object):

    @classmethod
    def format(cls, object_list):
        issues = sorted(object_list, key=lambda obj: obj.created_at, reverse=True)

        result = []
        for created_at, items in groupby(issues, key=lambda obj: obj.created_at.date()):
            groupby_date = {
                'created_at': created_at,
                'items': []
            }
            for location, subitems in groupby(sorted(items, key=lambda obj: obj.location), key=lambda obj: obj.location):
                groupby_location = {
                    'location': location,
                    'items': []
                }
                for issue in subitems:
                    groupby_location['items'].append(issue)
                groupby_date['items'].append(groupby_location)
            result.append(groupby_date)

        return result

