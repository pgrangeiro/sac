# coding: utf-8
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import FormView, ListView, View

from core.use_cases import CreateIssueUseCase, FinishIssueUseCase, IssueReportUseCase
from core.web_app.daos import IssueDao
from core.web_app.formatters import AggregateIssues
from core.web_app.forms import IssueForm
from core.web_app.models import Issue


class IssueFormView(FormView):

    form_class = IssueForm
    template_name = 'create_or_update_issue.html'
    success_url = '/'

    def get_form_kwargs(self):
        kwargs = super(IssueFormView, self).get_form_kwargs()

        issue_id = self.kwargs.get('issue_id')
        if issue_id:
            kwargs['instance'] = get_object_or_404(Issue, id=issue_id)
        return kwargs

    def form_valid(self, form):
        use_case = CreateIssueUseCase(IssueDao())
        data = form.cleaned_data
        data.pop('finished_at')

        use_case.execute(**data)

        return super(IssueFormView, self).form_valid(form)


class IssueListView(ListView):

    template_name = 'list_issue.html'

    def get_queryset(self):
        use_case = IssueReportUseCase(IssueDao(), AggregateIssues)
        return use_case.execute()


class FinishIssueView(View):

    def get(self, request, *args, **kwargs):
        issue_id = kwargs.get('issue_id')
        use_case = FinishIssueUseCase(IssueDao())

        use_case.execute(issue_id)

        return JsonResponse({})
