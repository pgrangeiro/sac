# coding: utf-8
from copy import deepcopy
from datetime import datetime

from core.web_app.models import Issue


class IssueMapper(object):

    @classmethod
    def format(cls, obj):
        return deepcopy(obj.__dict__)


class IssueDao(object):

    def create(self, source_id, location_id, subject_id, description):
        Issue.objects.create(
            source_id=source_id,
            location_id=location_id,
            subject_id=subject_id,
            description=description,
        )

    def all(self):
        for item in Issue.objects.all():
            yield IssueMapper.format(item)

    def finish(self, issue_id):
        Issue.objects.filter(id=issue_id).update(finished_at=datetime.now())
