# coding: utf-8
from datetime import date, datetime
from unittest import TestCase

from core.domain import Issue
from core.web_app.formatters import AggregateIssues


class AggregateIssuesTestCase(TestCase):

    def setUp(self):
        self.maxDiff = None

    def test_aggregate_issues_correctly(self):
        issues = [
            Issue(id=1, location_id=1, subject_id=1, source_id=1, created_at=datetime(2015, 1, 1, 8), description=''),
            Issue(id=2, location_id=1, subject_id=1, source_id=1, created_at=datetime(2015, 1, 1, 9), description=''),
            Issue(id=3, location_id=2, subject_id=1, source_id=1, created_at=datetime(2015, 1, 1, 8), description=''),
            Issue(id=4, location_id=2, subject_id=1, source_id=1, created_at=datetime(2015, 1, 2), description=''),
        ]
        expected = [{
            'created_at': date(2015, 1, 2),
            'items': [{
                'location': 'AL',
                'items': [issues[3]],
            }]
        }, {
            'created_at': date(2015, 1, 1),
            'items': [{
                'location': 'AC',
                'items': [issues[1], issues[0]],
            }, {
                'location': 'AL',
                'items': [issues[2]],
            }],
        }]

        result = AggregateIssues.format(issues)
        self.assertEqual(expected, result)
