# coding: utf-8
from datetime import datetime
from freezegun import freeze_time
from mock import call, patch, Mock
from model_mommy import mommy
from unittest import TestCase

from core.web_app.daos import IssueDao, IssueMapper
from core.web_app.models import Issue


class IssueDaoTestCase(TestCase):

    def setUp(self):
        patchers = [
            patch('core.web_app.daos.issue_dao.Issue', Mock(Issue)),
            patch('core.web_app.daos.issue_dao.IssueMapper', Mock(IssueMapper)),
        ]

        self.issue = patchers[0].start()
        self.mapper = patchers[1].start()
        self.dao = IssueDao()

        self.issue.objects.all.return_value = [1, 2]

        for p in patchers:
            self.addCleanup(p.stop)

    def test_create_calls_orm_correctly(self):
        self.dao.create(1, 2, 3, 'Description')
        self.issue.objects.create.assert_called_once_with(
            source_id=1,
            location_id=2,
            subject_id=3,
            description='Description',
        )

    def test_all_calls_orm_correctly(self):
        list(self.dao.all())
        self.assertTrue(self.issue.objects.all.called)

    def test_all_calls_mapper_correctly(self):
        self.mapper.format.side_effect = ['1', '2']

        result = list(self.dao.all())
        self.assertEqual(2, self.mapper.format.call_count)
        self.mapper.format.assert_has_calls([call(1), call(2)])
        self.assertEqual(['1', '2'], result)

    @freeze_time('2015-01-01 08:00')
    def test_finish_calls_orm_correctly(self):
        self.dao.finish(1)
        self.issue.objects.filter.assert_called_once_with(id=1)
        self.issue.objects.filter().update.assert_called_with(
            finished_at=datetime(2015, 1, 1, 8)
        )


class IssueMapperTestCase(TestCase):

    def test_formats_obj_correctly(self):
        obj = mommy.make(Issue)
        result = IssueMapper.format(obj)

        self.assertEqual(obj.id, result['id'])
        self.assertEqual(obj.source_id, result['source_id'])
        self.assertEqual(obj.location_id, result['location_id'])
        self.assertEqual(obj.subject_id, result['subject_id'])
        self.assertEqual(obj.description, result['description'])
        self.assertEqual(obj.created_at, result['created_at'])
        self.assertEqual(obj.finished_at, result['finished_at'])
