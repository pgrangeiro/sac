# coding: utf-8
from mock import patch, Mock

from django.core.urlresolvers import reverse
from django.test import TestCase

from core.web_app.forms import IssueForm


class IssueFormViewTestCase(TestCase):

    def setUp(self):
        self.url = reverse('core:manage_issues')

        self.use_case = patch('core.web_app.views.issue_view.CreateIssueUseCase').start()
        self.dao = patch('core.web_app.views.issue_view.IssueDao').start()

    def test_view_returns_200(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_view_has_correct_form_instance_in_context(self):
        response = self.client.get(self.url)

        form = response.context['form']
        self.assertIsInstance(form, IssueForm)

    @patch('core.web_app.views.issue_view.IssueReportUseCase.execute', Mock(return_value=[]))
    def test_view_redirects_to_correct_url_on_success(self):
        data = {
            'source_id': 1,
            'location_id': 2,
            'subject_id': 3,
            'description': u'Lorem Ipsum',
        }
        response = self.client.post(self.url, data)
        self.assertRedirects(response, reverse('core:list_issues'))

    def test_view_calls_use_case_on_success(self):
        data = {
            'source_id': 1,
            'location_id': 2,
            'subject_id': 3,
            'description': u'Lorem Ipsum',
        }
        response = self.client.post(self.url, data)
        self.use_case.assert_called_once_with(self.dao())
        self.use_case().execute.assert_called_once_with(**data)

    def test_view_uses_correct_template(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, 'create_or_update_issue.html')


class IssueListView(TestCase):

    def setUp(self):
        self.url = reverse('core:list_issues')

        self.use_case = patch('core.web_app.views.issue_view.IssueReportUseCase').start()
        self.formatter = patch('core.web_app.views.issue_view.AggregateIssues').start()
        self.dao = patch('core.web_app.views.issue_view.IssueDao').start()

    def test_view_returns_200(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_view_uses_correct_template(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response, 'list_issue.html')

    def test_view_calls_use_case_correctly(self):
        self.use_case().execute.return_value = [1]

        response = self.client.get(self.url)
        self.use_case.assert_called_with(self.dao(), self.formatter)
        self.assertTrue(self.use_case().execute.called)
        self.assertEqual([1], response.context['object_list'])


class FinishIssueViewTestCase(TestCase):

    def setUp(self):
        self.url = reverse('core:finish_issue', args=[1])

        self.use_case = patch('core.web_app.views.issue_view.FinishIssueUseCase').start()
        self.dao = patch('core.web_app.views.issue_view.IssueDao').start()

    def test_view_returns_200(self):
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response['Content-Type'])

    def test_view_calls_use_case_on_success(self):
        response = self.client.get(self.url)

        self.use_case.assert_called_once_with(self.dao())
        self.use_case().execute.assert_called_once_with('1')
