# coding: utf-8
from django.test import TestCase

from core.domain.collections import LOCATIONS, SOURCES, SUBJECTS
from core.web_app.forms import IssueForm


class IssueFormTestCase(TestCase):

    def setUp(self):
        self.data = {
            'source_id': 1,
            'location_id': 2,
            'subject_id': 3,
            'description': u'Lorem Ipsum',
        }
        self.form = IssueForm(self.data)

    def test_required_fields(self):
        form = IssueForm({})
        self.assertFalse(form.is_valid())

        self.assertIn(u'Este campo é obrigatório.', form.errors['source_id'])
        self.assertIn(u'Este campo é obrigatório.', form.errors['location_id'])
        self.assertIn(u'Este campo é obrigatório.', form.errors['subject_id'])
        self.assertIn(u'Este campo é obrigatório.', form.errors['description'])

    def test_form_source_field_has_corect_choices(self):
        for item in SOURCES:
            self.assertIn(item, self.form.fields['source_id'].choices)

    def test_form_location_field_has_corect_choices(self):
        for item in LOCATIONS:
            self.assertIn(item, self.form.fields['location_id'].choices)

    def test_form_subject_field_has_corect_choices(self):
        for item in SUBJECTS:
            self.assertIn(item, self.form.fields['subject_id'].choices)

    def test_form_is_valid(self):
        self.assertTrue(self.form.is_valid())
