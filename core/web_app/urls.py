from django.conf.urls import url

from core.web_app import views

urlpatterns = [
    url('^$', views.IssueListView.as_view(), name='list_issues'),
    url('^issues/$', views.IssueFormView.as_view(), name='manage_issues'),
    url('^issues/(?P<issue_id>\d+)/$', views.IssueFormView.as_view()),
    url('^issues/(?P<issue_id>\d+)/finish/$', views.FinishIssueView.as_view(), name='finish_issue'),
]
