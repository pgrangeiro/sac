# coding: utf-8
from django import forms

from core.domain.collections import LOCATIONS, SOURCES, SUBJECTS
from core.web_app.models import Issue


class IssueForm(forms.ModelForm):

    DEFAULT_CHOICE = [('', '--------')]

    source_id = forms.TypedChoiceField(label=u'Origem', choices=DEFAULT_CHOICE + SOURCES, coerce=int)
    location_id = forms.TypedChoiceField(label=u'Estado', choices=DEFAULT_CHOICE + LOCATIONS, coerce=int)
    subject_id = forms.TypedChoiceField(label=u'Assunto', choices=DEFAULT_CHOICE + SUBJECTS, coerce=int)
    description = forms.CharField(label=u'Descrição', max_length=1000, widget=forms.Textarea)
    finished_at = forms.CharField(label=u'Encerrado em', disabled=True, required=False)

    class Meta:
        model = Issue
        fields = ('source_id', 'location_id', 'subject_id', 'description', 'finished_at')
