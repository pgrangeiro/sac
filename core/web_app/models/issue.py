# coding: utf-8
from django.db import models


class Issue(models.Model):

    source_id = models.PositiveIntegerField()
    location_id = models.PositiveIntegerField()
    subject_id = models.PositiveIntegerField()
    description = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)
    finished_at = models.DateTimeField(null=True)
